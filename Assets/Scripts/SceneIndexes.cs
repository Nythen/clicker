﻿
public enum SceneIndexes
{
    MAIN = 0,
    COMBAT = 1,
    LOOT = 2,
    INVENTORY = 3
}
